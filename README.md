# Memory Manager
Simulação de gerenciador de Memória com listas duplamente encadeadas.

# Authors
Erick José Heiler, Halena Kulmann Duarte e Éder Augusto Penharbel

# Resumo
https://drive.google.com/file/d/15KjEVuK2x94KvjGWo66UWdMM7pm934h2/view?usp=sharing

# Relatório
https://drive.google.com/file/d/1paqyIjhJEOtY_K_it5d7bQzGu4cuM1uK/view?usp=sharing
